﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using StudentDataAcces;

namespace UniversidadService.Controllers
{
    public class StudentController :ApiController
    {
        public IEnumerable<students> Get()
        {
            using (uniEntities entities = new uniEntities())
            {
                return entities.students.ToList();
            }
        }

        public students Get(int id)
        {
            using (uniEntities entities = new uniEntities())
            {
                return entities.students.FirstOrDefault(e => e.Id == id);
            }
        }

        public void Post([FromBody] students student)
        {
           // try
            //{
                using (uniEntities entities = new uniEntities())
                {
                    entities.students.Add(student);
                    entities.SaveChanges();

                  //  var message = Request.CreateResponse(HttpStatusCode.Created, student);
                    //message.Headers.Location = new Uri(Request.RequestUri + student.Id.ToString());
                }
            //}
            //catch(Exception ex)
            //{
              //  return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            //}
        }

        public void Delete (int id)
        {
            using (uniEntities entities = new uniEntities())
            {
                entities.students.Remove(entities.students.FirstOrDefault(e => e.Id == id));
                entities.SaveChanges();
            }
        }

        public void Put(int id, [FromBody] students student)
        {
            using (uniEntities entities = new uniEntities())
            {
                var entity = entities.students.FirstOrDefault(e => e.Id == id);

                entity.Ci = student.Ci;
                entity.FirstName = student.FirstName;
                entity.LastName = student.LastName;
                entity.Career = student.Career;

                entities.SaveChanges();
            }
        }
    }
}